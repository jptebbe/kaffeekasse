///////////////////
////
////     AUTOMATIC COFFEE BILL
////        - a Google Spreadsheet/Formular based system to calcualte the coffee bill

/// TODO: formatting and colours of the sheet


///////////////
//  A. Get Credentials & global variables
var ss = SpreadsheetApp.openById("18pb_r8Yw17EezoESuS2gNzlSJ5ZlOQQeJyEOudzDbbs");
var sheetFormular = ss.getSheetByName("Formularantworten");
var coffePrice = 0.15;
    // eg. "PayPal.Me/youraccount/13,05" ==> payPalName = "youraccount"
var payPalName =  "kauflandkaffee";
var monthList = [];
var billStructure = [];
var monthMapping = [
    "01",
    "02",
    "03",
    "04",
    "05",
    "06",
    "07",
    "08",
    "09",
    "10",
    "11",
    "12"
]

///////////////
//  B. Help Functions
    // check if array is in object
function contains(a, obj) {
    var i = a.length;
    while (i--) {
        if (String(a[i]) === String(obj)) {
            return true;
        }
    }
    return false;
}
    // create Names List
function createNamesList () {
    var answers = "B2:B" + sheetFormular.getLastRow();
        // check for name
    var names = sheetFormular.getRange(answers).getValues();
    var count = names.length;
    var namesList = [];

    for (var i = 0; i < count; i++) {        
        if (! contains(namesList, names[i]) ) {
            namesList.push(names[i]);
        }
    }
    return namesList;
}
    // compare Year and Month of sheet and log
function compareYearAndMonth (lY, lM, sY, sM) {
    if ( String( lY + lM ) === String( sY + sM ) ) {
        return true;
    } else{
        return false;
    }
}
    // check which months are in the logs
function checkMonthsCount (checkLogList) {
    var count = checkLogList.length;
    var prevYear = 0;
    var prevMonth = 0;
    
    for (var i = 0; i < count; i++) {
        var makeDate = new Date (checkLogList[i]);
        var curYear = makeDate.getFullYear();
        var curMonth = makeDate.getMonth();
  
        var checkIfDateIsAlreadyThere = compareYearAndMonth(prevYear, prevMonth, curYear, curMonth);
        if ( (! checkIfDateIsAlreadyThere) ) {
            monthList.push({
                "month" : curMonth,
                "year" : curYear
            });
        }
        prevYear = curYear
        prevMonth = curMonth;
    }
    return monthList;
}

///////////////
//  C. Calculate Bill
    // create a bill sheet structure - one for every month
function createBillStructure () {
    // prepare logs
    var formularLength = sheetFormular.getLastRow();
    var dateLogs = "A2:A" + formularLength;
    var dates = sheetFormular.getRange(dateLogs).getValues();
    var nameLogs = "B2:B" + formularLength;
    var names = sheetFormular.getRange(nameLogs).getValues();
        // check how many sheets are needed for which year/month
    var months = checkMonthsCount ( dates );
    var countMonths = months.length;        
        // check on how many unique names are are one the list
    var allNames = createNamesList();
    for (var i = 0; i < countMonths; i++) {
            // get names for all to-be-created sheets
        var formattedMonth = monthMapping[ months[i].month ];
        var sheetName = months[i].year + "-" + formattedMonth;
            // bill structure for one month => everything except of the specific amount of coffees
            // or if that person drank coffee there at all
        if (! contains(billStructure, sheetName ) ) {
            var format = {
                "sheetName": sheetName,
                "content": [
                ]
            };
                // push all names (min 1 coffee) into that month
            for (var n = 0; n < allNames.length; n++) {
                var specificDebtor = {
                    "name" : String(allNames[n]),
                    "coffeeAmount" : 0
                };
                format.content.push(specificDebtor);
            }
                // push months bill into complete bill
            billStructure.push(format);
        }
            // figure out if person drank coffee and how many
        for (var y = 0; y < formularLength - 1; y++) {
            // check if relevant month
                // compares logline to current bill month
            var logDate = new Date (dates[y]);
            var lY = logDate.getFullYear();
            var lM = logDate.getMonth();

            var checkCurrentMonth = ( compareYearAndMonth (lY, lM, months[i].year, months[i].month) );    
            if (checkCurrentMonth) {
                // result: yes: logline is for current month              
                for (var p = 0; p < billStructure[i].content.length; p++) {
                        // which name is in the logline?
                        // search name in structure and add +1 in cofffee
                    if ( String(names[y]) === String(billStructure[i].content[p].name) ) {
                        billStructure[i].content[p].coffeeAmount = billStructure[i].content[p].coffeeAmount + 1;
                        break;
                    }
                }
            }
        } 
    }
  return billStructure;
}

function createDesign (currentSheet, allData, rows) {
        // the basic sheet structure
        var title = currentSheet.getRange("A1:G1").mergeAcross().setValue("Kaffee Abrechnung");
        var subtitle = currentSheet.getRange("A2:G2").mergeAcross().setValue("Zeitraum " + allData[i].sheetName);
        currentSheet.getRange("A3").setValue("Name");
        currentSheet.getRange("B3").setValue("Kaffee Menge");
        currentSheet.getRange("C3").setValue("Diesen Monat");
        currentSheet.getRange("D3").setValue("Gezahlt (letzter Monat)")
        currentSheet.getRange("E3").setValue("Übertrag");
        currentSheet.getRange("F3").setValue("Gesamt Offen");  
        currentSheet.getRange("G3").setValue("Links");
            // which colours will be used
        var colorTop = "#e34e65";
        var colorLeft = "#2d393d";
        var colorContent = "#f1e1cf";
            // how many rows are used by the description
        var topRows = 3;
        var cellTop = currentSheet.getRange("A1:G3").setBackground(colorTop);
        var cellLeft = currentSheet.getRange("A4:A" + (rows + topRows) ).setBackground(colorLeft);
        var cellContent = currentSheet.getRange("B4:G" + (rows + topRows) ).setBackground(colorContent);
            // font settings
        title.setFontColor(colorLeft).setFontFamily("Helvetica").setFontSize(20).setFontWeight("bold").setHorizontalAlignment("center");
        subtitle.setFontColor(colorLeft).setFontFamily("Helvetica").setFontSize(14).setFontWeight("bold").setHorizontalAlignment("center");
        var topDescription = currentSheet.getRange("A3:G3").setFontColor(colorContent).setFontWeight("bold").setHorizontalAlignment("center");
        cellLeft.setFontColor(colorContent).setFontStyle("italic");
        cellContent.setFontStyle("italic");
        var moneyCells = currentSheet.getRange("C4:F").setNumberFormat("0.00 €");
        var totalToPay = currentSheet.getRange("F4:F").setFontStyle("normal");
            // Resize cells
        currentSheet.autoResizeColumn(7);
        currentSheet.getRange("A1:F3").setWrap(true);
        
}
    
///////////////
//  D. CreateSheets
function createBillSheets () {
        // create data
    var allData = createBillStructure ();
        // get sheets in a list
    var allSheets = ss.getSheets();
    var sheetList = [];
    if (allSheets.length > 1) {
        for (i = 0; i < allSheets.length; i++){
            Logger.log(allSheets[i].getName());
            sheetList.push(allSheets[i].getName());
        }       
    }
        // iterate month by month inside the data
    for (i = 0; i < allData.length; i ++) {
            // check if sheet already exists
        if (contains(sheetList, allData[i].sheetName) ) {
            Logger.log("Yes "+ allData[i].sheetName + "exists. I didn't need to create it.")
        }else {
            ss.insertSheet(allData[i].sheetName);
             Logger.log("Nein "+ allData[i].sheetName + " didn't exist. I created it!")
        }
        
            // prepare sheet
        currentSheet = ss.getSheetByName(allData[i].sheetName);
        var rows = allData[i].content.length;
        Logger.log(rows)
        var currentRow = "4";
            // Set up design
        createDesign (currentSheet, allData, rows);
            // add names, coffee and prices
        for (x = 0; x < rows; x++) {
            currentSheet.getRange("A" + currentRow).setValue(allData[i].content[x].name);
            currentSheet.getRange("B" + currentRow).setValue(allData[i].content[x].coffeeAmount);
            currentSheet.getRange("C" + currentRow).setValue(allData[i].content[x].coffeeAmount * coffePrice);
                // in order to calculate the bill transfers from month to month the previous sheet is needed
            var oldCalcData = "";
            if (i != 0) {
                oldCalcData = allData[i-1];
                currentSheet.getRange("E" + currentRow).setValue("=\'" + oldCalcData.sheetName + "\'!F" + currentRow + " - D" + currentRow);
            }
            currentSheet.getRange("F" + currentRow).setValue("=C" + currentRow + " + E" + currentRow );
            currentSheet.getRange("G" + currentRow).setValue("=CONCATENATE(\"PayPal.Me/" + payPalName + "/\";F"+ currentRow +")");
            currentRow++;
       }
       
    }  
}
    
    // DEBUG FUNCTIONS
function debugFunction () {
    Logger.log ( "debugFunction ::: START: \n ");
//    Logger.log ("NO FUNCTION SET" );
        Logger.log ( createBillStructure () );
    Logger.log ( "debugFunction ::: END.\n");
}